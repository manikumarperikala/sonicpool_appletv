//
//  ASSearchListViewController.swift
//  Sonic Pool
//
//  Created by Sanchan on 20/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit

class SPSearchListViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UISearchResultsUpdating,UISearchBarDelegate,myListdelegate {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var searchController : UISearchController!
    var filteredDataItems = Array<Any>()
    var searchCollectionList = NSMutableArray()
    var userId = String()
    var deviceId,uuid: String!
    var searchDelegate: myListdelegate!
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        searchBar.barTintColor = UIColor.white
        let lists:Array = searchCollectionList as Array
        let searchPredicate = NSPredicate(format: "name CONTAINS[C] %@", searchText)
        let array = (lists as NSArray).filtered(using: searchPredicate)
        filteredDataItems = array
        collectionView.reloadData()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        filteredDataItems = searchCollectionList as Array
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return filteredDataItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let imagePath = filteredDataItems[indexPath.row] as! NSDictionary
        let image = imagePath["metadata"] as! NSDictionary
        (cell.viewWithTag(20) as! UILabel).text = image["title"] as? String
        (cell.viewWithTag(21) as! UIImageView).kf.setImage(with: URL(string: image["movie_art"] as! String))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        if let prev = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: prev)
        {
            cell.transform = .identity
        }
        if let next = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: next)
        {
            (cell.viewWithTag(21) as! UIImageView).adjustsImageWhenAncestorFocused = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let Path = filteredDataItems[indexPath.row] as! NSDictionary
        getaccountInfo(id:Path["id"] as! String,userid: userId/*,tvshow: (Path["tv_show"] as! Bool)*/)
        
        // getAssetData(withUrl:kAssestDataUrl,id: Path["id"] as! String,userid: userId,tvshow:(Path["tv_show"] as! Bool))
    }
    
    func updateSearchResults(for searchController: UISearchController)
    {
        searchController.searchBar.delegate = self
    }
    
    func getaccountInfo(id:String,userid:String/*tvshow:Bool*/)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAccountInfo":["deviceId":deviceId as AnyObject,"uuid":uuid as AnyObject]]
        print(parameters)
        SPApiManager.sharedManager.postDataWithJson(url: kAccountInfoUrl, parameters: parameters){
            (responseDict,error,isDone) in
            if error == nil
            {
                let Json = responseDict
                let dict = Json as! NSDictionary
                accountresponse = (dict.value(forKey: "uuid_exist") as! Bool)
                if accountresponse == true
                {
                    self.getAssetData(withUrl:kAssestDataUrl,id: id,userid: userid/*,tvshow:tvshow*/)
                }
                else
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.gotoCode()
                }
            }
        }
        
    }
    
    func getAssetData(withUrl:String,id:String,userid:String/*,tvshow:Bool*/)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAssestData":["videoId":id as AnyObject,"userId":userid as AnyObject,"returnType":"tiny" as AnyObject]]
        SPApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let DetailPage = storyBoard.instantiateViewController(withIdentifier: "DetailPage") as! SPDetailPageViewController
                let jsonresponse = JSON as! NSArray
                for dict in jsonresponse
                {
                    DetailPage.TvshowPath = dict as! NSDictionary
                }
                DetailPage.userid = self.userId
                DetailPage.deviceId = self.deviceId
                DetailPage.uuid = self.uuid
                DetailPage.delegate = self.searchDelegate
                self.present(DetailPage, animated: true, completion: nil)
                //self.navigationController?.pushViewController(DetailPage, animated: true)
            }
            else
            {
                print("json Error")
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                    self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
extension SPSearchListViewController
{
    func myListdata(mylistDict: NSDictionary) {
        
    }
    func removeListdata(id : String) {
        
    }
    func recentlyWatcheddata(recentlyWatchedDict: NSDictionary) {
        
    }
}
